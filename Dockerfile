FROM alpine:latest
RUN apk add --update nodejs npm
RUN npm install -g serve
WORKDIR /home/app
COPY build/ /home/app
ENTRYPOINT ["serve","-s","/home/app"]

